﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;
using UnityEngine.UI;

public class UserProfileInfo : MonoBehaviour
{
    public Text steamName, steamID;
    public RawImage profileImg;

    public void Start()
    {
        if (SteamManager.Initialized)
        {
            steamName.text = SteamFriends.GetPersonaName() + "/n" + SteamFriends.GetPersonaState() + "/n" + SteamUser.GetPlayerSteamLevel();
            Debug.Log(SteamFriends.GetPersonaName() + "/n" + SteamFriends.GetPersonaState() + "/n" + SteamUser.GetPlayerSteamLevel());

            CSteamID steamId = SteamUser.GetSteamID();
            steamID.text = SteamUser.GetSteamID ().ToString();
            Debug.Log(steamId);

           profileImg.texture = GetSmallAvatar(steamId);


        }

    }


    public Texture2D GetSmallAvatar(CSteamID user)
    {
        int FriendAvatar = SteamFriends.GetSmallFriendAvatar (user);
        uint ImageWidth;
        uint ImageHeight;
        bool success = SteamUtils.GetImageSize (FriendAvatar, out ImageWidth, out ImageHeight);

        if (success && ImageWidth > 0 && ImageHeight > 0)
        {
            byte[] Image = new byte[ImageWidth * ImageHeight * 4];
            Texture2D returnTexture = new Texture2D ((int)ImageWidth, (int)ImageHeight, TextureFormat.RGBA32, false, true);
            success = SteamUtils.GetImageRGBA (FriendAvatar, Image, (int)( ImageWidth * ImageHeight * 4 ));
            if (success)
            {
                returnTexture.LoadRawTextureData (Image);
                returnTexture.Apply ();
            }
            return returnTexture;
        }
        else
        {
            Debug.LogError ("Couldn't get avatar.");
            return new Texture2D (0, 0);
        }
    }
}

